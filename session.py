import time
import random
import string

class Session:
    'Session class holds all active sessions'
    sessions ={}
    TIMEOUT = 30
    
    __session_id = None
    __values = None
    __time = None

    def __init__(self,user):
        self.values ={}
        self.values['user'] = user
        self.session_id = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(128))
        self.time = time.time()
        Session.sessions[self.session_id]=self  
    @staticmethod
    def get(id):
        return Session.sessions[id]
    def set_value(self,vType,value):
        self.values[vType]=value
    def get_value(self,value):
        return self.values.get(value)
    def get_Time(self):
        return self.time
    def close(self):
        Session.sessions.remove(self)
    def getSessionId(self):
        return self.session_id
    def is_active(self):
        if (self.get_Time() - time.time())  > Session.TIMEOUT:
            Session.sessions.remove(self.session_id);
            print(self.get_Time() - time.time())
            return False
        else:
            self.renew_lease()
            print(self.get_Time() - time.time())
            return True
    def renew_lease(self):
        self.time = time.time()

