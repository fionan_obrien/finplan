<!DOCTYPE html>
<html lang="en">
	<head> 
		%include('_template/scripts')
	</head>
	
	<body>
		%include('_template/navbar')
											<!--   This is where your HTML code STARTS -->
		<div class="container">
		  <h1>Expenditure Details</h1>
		  <script>var  EXPEND_TYPES =['Mortgage Expense','Living Expense','Health Insurance','Car Loans','Personal Loans','Other'];
		  
		
			var EXPEND_COUNTER =0;
			var DATE_PATTERN = "^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$";
		  	$(document).ready(function(){
				var options = $("#types_of_expenditure");
				var restartButton  = $("#Restart_Button");
				
				restartButton.hide();
				
				for(n in EXPEND_TYPES){
					options.append("<option>"+EXPEND_TYPES[n]+"</option>");
				}
				
				restartButton.click(function(){
					location.reload();
				});
				
				$("#new_expenditure_button").click(function(){
					add_new_source();
					restartButton.show();
				});
				
			
				$("#del").click(function(e){
					e.preventDefault();
					console.log("SUP");
				});
			});
			
			
			
			function add_new_source(){
			
				var source_type = $("#types_of_expenditure option:selected").text();
				var form  = $("#source_of_expenditure_form");
				var $div = $("<div>", {id: "expenditureBlock_"+EXPEND_COUNTER++});
				var $heading = $('<h3>');
				
				$heading.text(""+source_type);
				
				$div.append($heading);
				
				$div.append(
					$('<div/>')
						.addClass('well well-lg')
						.append(
							$('<div/>')
								.addClass("form-group")
								.append(
									$('<label/>')
									.text("Name:")
								)
								.append(
									$('<br/>')
									)
								.append(
									$('<input/>')
									.attr('type','text')
									.attr("name","name_"+EXPEND_COUNTER)
									.attr("placeholder","Enter Name or brief description")
									.attr("required","")
									.addClass("form-control")
									)
								.append(
									$('<label/>')
									.text("Amount:")
								)
								.append(
									$('<br/>')
									)
								.append(
									$('<input/>')
									.attr('type','number')
									.attr("id","amount_"+EXPEND_COUNTER)
									.attr("name","amount_"+EXPEND_COUNTER)
									.attr("placeholder","€")
									.addClass("form-control")
									)
							
						)
							.append(
								$('<div/>')
									.addClass('form-group')
									.append(
									$('<label>')
									.attr("for","frequency")
									.text("Frequency: ")
									)
									
									.append(
									$('<br/>')
									)
									
									.append(
										$('<input\>')
										.attr('type','radio')
										.attr('name','frequency_'+EXPEND_COUNTER)
										.attr('value','Recurring')
										.on("click",function(){
											$("#enddate"+EXPEND_COUNTER).show()
											
											})
										
									)
									.append(' Recurring ')
									.append(
									$('<br/>')
									)
									.append(
										$('<input>')
										.attr('type','radio')
										.attr('name','frequency_'+EXPEND_COUNTER)
										.attr('value','one-off')
										.on("click",function(){
											$("#enddate"+EXPEND_COUNTER).hide()
											
											})
										
									)
									.append(' Once-off ')
							)						
						.append(
							$('<div/>')
								.addClass("form-group")
								.append(
									$('<label/>')
									.text("Duration:")
								)
								.append(
									$('<br/>')
									)
								.append(
								$('<input/>')
								.attr('type','date')
								.attr("id","startdate"+EXPEND_COUNTER)
								.attr("placeholder","Start")
								.attr("pattern",DATE_PATTERN)
								.attr("name","DurationStart_"+EXPEND_COUNTER)
								.addClass("form-control")
								)
								.append(
									$('<input/>')
									.attr('type','date')
									.attr("id","enddate"+EXPEND_COUNTER)
									.attr("placeholder","End date")
									.attr("pattern",DATE_PATTERN)
									.attr("name","DurationEnd_"+EXPEND_COUNTER)
									.addClass("form-control")
								)

					
					)
				
				
				);
				$("#counter").attr("value",EXPEND_COUNTER);
				form.prepend($div);
				form.validator('update');
				
				
			}
		  
		  </script>
		  
		  
			<select class="form-inline form-control" id='types_of_expenditure' placeholder="Add New expenditure">

			</select>
		  <button id='new_expenditure_button' class ='form-control btn-success'>Add</button>
		  <button id='Restart_Button'  class ='form-control btn-danger'>Clear All</button>
		  <form id='source_of_expenditure_form' data-toggle='validator' method ="POST"  name ="expenditure_sources" action="/Insert_expenditure_details">
		  
		  
		  <br><br><button type="submit" class="btn btn-default">Continue</button><br><br>
		  <input type="hidden" id="counter" value=0 name ="counter"></input>
		</form>
		</div>
		<!--   This is where your HTML code ENDS -->
	</body>
</html>