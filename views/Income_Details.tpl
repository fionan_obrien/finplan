<!DOCTYPE html>
<html lang="en">
  <head>
		%include('_template/scripts')
	  </head>
	  <body>
		%include('_template/navbar')
		<!--   This is where your HTML code STARTS -->
    <script>

        $(document).ready(function(){
        var INPUT_TYPES =['Employment','Pension','Shares','Maintenance','Social Welfare','Dividends','Awards from Claim and Employment'];
        var form_template = $(".form-block");
        var COUNTER =0;
        var options = $("#types_of_income");
        var restartButton  = $("#Restart_Button");
        restartButton.hide();

        //load in income types.
        for(n in INPUT_TYPES){

        options.append("<option>"+INPUT_TYPES[n]+"</option>");
        }

        $("#new_income_button").click(function(e){
        var source_type = $("#types_of_income option:selected").text();
		COUNTER++;
		updateCounter();
        add_new_income(source_type);
        restartButton.show();
        
        });	
        restartButton.click(function(e){
        e.preventDefault();
        var source_type = $("#types_of_income option:selected").text();
        $("#source_of_income_form").find('*').not("#submit_button, #counter").remove();
		$("#source_of_income_form").prepend("<br>");
        restartButton.hide();
        COUNTER=0;
		updateCounter();
        });	
		
		

        function add_new_income(INPUT_TYPE){

        var new_form_block = form_template.clone();

        //change heading
        new_form_block.find("h3").text(INPUT_TYPE);

        //change name input
        new_form_block.find("#name").attr("name","name_"+COUNTER);
        //change amount
        new_form_block.find("#amount").attr("name","amount_"+COUNTER);
        //change frequency
        new_form_block.find("#Recurring").attr("name","frequency_"+COUNTER);
        new_form_block.find("#Once-off").attr("name","frequency_"+COUNTER);
		console.log(new_form_block.find("#Once-off").attr("name","frequency_"+COUNTER));
        //startdate name
        new_form_block.find("#startdate").attr("name","DurationStart_"+COUNTER);
        //enddate name
        new_form_block.find("#enddate").attr("name","DurationEnd_"+COUNTER);
        //make visible
        new_form_block.show();


        //add functionality to buttons
        new_form_block.find("#remove_button").click(function(e){
        e.preventDefault();
        this.closest(".form-block").remove();
        COUNTER--;
		updateCounter();
        });
        //set up frequency
        new_form_block.find("#Once-off").click(function(e){
        $(this).parent().find("#enddate").hide();

        });
        new_form_block.find("#Recurring").click(function(e){
        $(this).parent().find("#enddate").show();

        });

        var form_master =  $("#source_of_income_form");
        form_master.prepend(new_form_block);

        }
		
		function updateCounter(){
		$("#counter").attr("value",COUNTER);
		}
		

        });

    </script>
    
    
    
    <div class="container">
        <h1>Income Details</h1>
        <div class="form-inline">
            <select class="form-control col-md-12" id='types_of_income'></select>

            <button  id='new_income_button' type="button" class="btn btn-success btn-add  col-md-2" ><span class="glyphicon glyphicon-euro"></span> Add Income Source</button>  
            <button id='Restart_Button'  class ='form-control btn-danger  col-md-2' >Clear All</button>
            <br>
        </div>
        <!-- MAIN FORM -->
        <form id='source_of_income_form' data-toggle='validator' method ="POST"  action="/Insert_income_details">
            <input type="hidden" id="counter" value=0 name ="counter"/>
			<br>
			  <button id= "submit_button"type="submit" class="btn btn-default">Continue</button>
        </form>
        <!-- MAIN FORM END -->

        <!-- TEMPLATE START -->
        <div class="form-block" hidden>
            <h3 id="heading">HEADING</h3><button id="remove_button" class="btn btn-danger glyphicon glyphicon-remove pull-right"></button>
            <div class='well well-lg'>	
                <div class='form-group'>
                    <label>Name:</label><br>
                    <input type="text" class="form-control" id="name" placeholder="Enter Name or brief description" required/><br>
                    <label>Amount:</label><br>
                    <input type="number" class="form-control" id="amount" placeholder="&euro;" required>
                </div>
                <div class="form-group">
                    <label for="frequency">Frequency: </label><br>
                    <input name="frequency" value="Recurring" type="radio" id="Recurring"> Recurring <br>
                    <input name="frequency" value="Once-off" type="radio" id="Once-off"> Once-off<br>
                    <label>Duration:</label><br>
                    <input type="date" class="form-control" id="startdate" name="startdate" placeholder="start" pattern="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                    <br>
                    <input type="date" class="form-control" id="enddate"  name="enddate" placeholder="end" pattern="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                </div>
            </div>

        </div>
        <!-- TEMPLATE END -->
    </div>		
		
		<!--   This is where your HTML code ENDS -->
</body>
</html>