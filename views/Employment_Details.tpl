<!DOCTYPE html>
<html lang="en">
  <head>
		%include('_template/scripts')
	  </head>
	  <body>
		%include('_template/navbar')
		<!--   This is where your HTML code STARTS -->
		<div class="container">
			<h1>Employment Details</h1>
		  
			<h3> <p id="client_id"></p>
				<script>document.getElementById("client_id").innerHTML = "Client Id: test id";
				</script>
			</h3>
			<br>
		  
			<script>var  EMPLOY_TYPES =['Current Employment','Previous Employment'];
		  
		
				var EMPLOY_COUNTER =0;
				var DATE_PATTERN = "^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$";
				
				$(document).ready(function(){
				
					var options = $("#types_of_employ");
					var restartButton  = $("#Restart_Button");
				
					restartButton.hide();
				
					for(n in EMPLOY_TYPES){
						options.append("<option>"+EMPLOY_TYPES[n]+"</option>");
					}
				
				
				
					restartButton.click(function(){
				
						location.reload();
					});
				
					$("#new_employ_button").click(function(){
				
						add_new_source();
						restartButton.show();
					});
		
			
					$("#del").click(function(e){
				
						e.preventDefault();
						console.log("SUP");
				});
			});
			
			
			
				function add_new_source(){
				
					var source_type = $("#types_of_employ option:selected").text();
					
					var form  = $("#source_of_employ_form");
					
					var $div = $("<div>", {id: "employBlock_"+EMPLOY_COUNTER++});
					var $heading = $('<h3>');
					
					$heading.text(""+source_type);
					
					$div.append($heading);
					
					$div.append(
						$('<div/>')
							.addClass('well well-lg')
							.append(
								$('<div/>')
									.addClass("form-group")
									.append(
										$('<label/>')
										.text("Name:")

									)
									.append(
										$('<br/>')
										)
									.append(
										$('<input/>')
										.attr('type','text')
										.attr("name",'ename_'+EMPLOY_COUNTER)
										.attr("placeholder","Enter Employer Name")
										.attr("required","")
										.addClass("form-control")
										)
										
										.append(
										$('<br/>')	
									)
									.append($('<label/>')
										.text("Employer Address:")
									)
									.append(
										$('<br/>')
									)
									.append(
										$('<input/>')
										.attr('type','text')
										.attr("name",'address_'+EMPLOY_COUNTER)
										.attr("placeholder","Enter Employer Address")
										.attr("required","")
										.addClass("form-control")
									)
									.append(
										$('<br/>')
									)
									.append(
										$('<label/>')
										.text("Employer Contact Phone Number:")
									)
									.append(
										$('<br/>')
									)
									.append(
										$('<input/>')
										.attr('type','tel')
										.attr("name",'telno_'+EMPLOY_COUNTER)
										.attr("placeholder","Enter Contact Phone Number: xxx-xxxxxxx")
										.addClass("form-control")
									)
									.append(
										$('<br/>')
									)
									.append(
										$('<label/>')
										.text("Employer Email Address:")
									)
									.append(
										$('<br/>')
									)
									.append(
										$('<input/>')
										.attr('type','email')
										.attr("name",'eemail_'+EMPLOY_COUNTER)
										.attr("placeholder","Enter Contact Email address")
										.addClass("form-control")
									)
									.append(
										$('<br/>')
									)
									
								
							)
								.append(
									$('<div/>')
										.addClass('form-group')
										.append(
										$('<label>')
										.attr("for","duration")
										.text("Duration: ")
										)
										
										.append(
										$('<br/>')
										)
										
										.append(
											$('<input\>')
											.attr('type','radio')
											.attr('name','etypes_'+EMPLOY_COUNTER)
											.attr('value','contract')
											.on("click",function(){
												$("#startdate"+EMPLOY_COUNTER).show()
												
												})
											
										)
										.append(' Contract ')
										.append(
										$('<br/>')
										)
										
										.append(
											$('<input>')
											.attr('type','radio')
											.attr('name','etypes_'+EMPLOY_COUNTER)
											.attr('value','permanent')
											.on("click",function(){
												$("#enddate"+EMPLOY_COUNTER).show()
												
												})
											
										)
										.append(' Full-time ')
								)						
							.append(
								$('<div/>')
									.addClass("form-group")
									.append(
										$('<label/>')
										.text("Duration:")
									)
									.append(
										$('<br/>')
									)
									.append(
									$('<input/>')
									.attr('type','date')
									.attr('name','startdate_'+EMPLOY_COUNTER)
									.attr("pattern",DATE_PATTERN)
									.addClass("form-control")
									)
									.append(
										$('<input/>')
										.attr('type','date')
										.attr('name','enddate_'+EMPLOY_COUNTER)
										.attr("pattern",DATE_PATTERN)
										.addClass("form-control")
									)
						)
					);
					$("#counter").attr("value",EMPLOY_COUNTER);
					form.prepend($div);
					form.validator('update');
					
					
				}
		  
			</script>
		  
		<select class="form-inline form-control" id='types_of_employ' placeholder="Add Employment">
		</select>
		<button id='new_employ_button' class ='form-control btn-success'>Add</button>
		<button id='Restart_Button'  class ='form-control btn-danger'>Clear All</button>
		<form id='source_of_employ_form' data-toggle='validator' method ="POST"  name ="employ_sources" action="/Insert_employment_details">		

		<br><br><button type="submit" class="btn btn-default">Continue</button><br><br>
		<input type="hidden" id="counter" value=0 name ="counter"></input>
		</form>
		</div>

</body>
</html>