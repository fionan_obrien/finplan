<!DOCTYPE html>
<html lang="en">
  <head>
		%include('_template/scripts')
	  </head>
	  <body>
		%include('_template/navbar')
		
		<!--   This is where your HTML code STARTS -->
		<div class="container">
		  <h1>Add Client</h1>
		  <p>Enter contact details for a new client</p> 

		    <form role="form" data-toggle="validator" action="/newclientdetails" method="POST">
		  <div class="form-group">
			<label for="fmail">First Name:</label>
			<input type="fname"  required class="form-control" id="fname" name="fname" placeholder="First Name" >
		  </div>
		  <div class="form-group">
			<label for="lname">Last Name:</label>
			<input type="lname" placeholder="Last Name" class="form-control" id="lname" name="lname" required>
		  </div>
		  <div class="form-group">
			<label for="addln1">Address Line 1:</label>
			<input type="addln1" placeholder="Address Line 1" class="form-control" id="addln1" name="addln1" required>
		  </div>
		  <div class="form-group">
			<label for="addln2">Address Line 2:</label>
			<input type="addln2" placeholder="Address Line 2" class="form-control" id="addln2" name="addln2">
		  </div>
		  <div class="form-group">
			<label for="town">Town:</label>
			<input type="town" placeholder="Town" class="form-control" id="town" name="town">
		  </div>
		  <div class="form-group">
			<label for="county">County:</label>
			<input type="county" placeholder="County" class="form-control" id="county" name="county">
		  </div>
		  <div class="form-group">
			<label for="eircode">Eircode:</label>
			<input type="eircode" placeholder="Eircode" class="form-control" id="eircode" name="eircode" data-minlength="7">
		  </div>
		  <div class="form-group">
			<label for="mphone">Mobile Phone Number:</label>
			<input type="tel" placeholder="xxx-xxxxxxx" class="form-control" id="mphone" name="mphone" pattern="\b\d{3}[-.]?\d{3}[-.]?\d{4}\b">
		  </div>
		  <div class="form-group">
			<label for="ophone">Other Phone Number:</label>
			<input type="tel" class="form-control" id="ophone" name="ophone">
		  </div>
		  <div class="form-group">
			<label for="email">Email address:</label>
			<input type="email" class="form-control" id="email" name="email">
		  </div>
		  <div class="form-group">
			<label for="dob">Date of Birth:</label>
			<input type="date" class="form-control" id="dob" name="dob" placeholder="DD/MM/YYYY" pattern="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">

		  </div>
		  <div class="form-group">
			<label for="pwd">Gender:</label><br>
			<input type="radio" name="gender" value="m" checked> Male<br>
			<input type="radio" name="gender" value="f"> Female<br>
			</div>
		  <button type="submit" class="btn btn-default">Submit
		  </button>
		  
		</form>
		</div>
		<!--   This is where your HTML code ENDS -->
</body>
</html>