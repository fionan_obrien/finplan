from bottle import route, run, template, get, post, request, view, static_file,response
from databasefunctions import Client
import datetime

#database
import sqlite3

#session class for navigation
from session import Session

global PAGES
PAGES =['Client Search','Add Client','Employment Details','Income Details','Expenditure Details','Product Types' ,'Product Details']

#simpllfy session creation
@route('/home')
def home():
	username = 'GUEST'
	new_session = Session(username)
	#default values for testing purposes
	new_session.set_value('user_id',1)
	new_session.set_value('client_id',1)
	new_session.set_value('product_id',1)
	
	response.set_cookie('finplan_cookie',new_session.getSessionId(),max_age=3600)
	picture_name = 'front_img.jpg'
	return template('home', picture = picture_name)

@route('/img/<picture>')
def serve_pictures(picture):
    return static_file(picture, root='img/')
#main entry to program
@route('/finplan')
def finplan(LOGGED_IN=False):
	cookie = find_cookie()
	if not LOGGED_IN :
		return check_for_cookie(cookie)
	else: 
		return template('template')

#Session maintance
def check_for_cookie(cookie):
	if cookie is None:
		return login()
	if cookie in Session.sessions:
		s  = find_session(cookie)
		if is_session_active(s):
			return finplan(True)
	else:
		return login()

def find_cookie():
        return request.get_cookie('finplan_cookie')

def find_session(cookie):
        return Session.get(cookie)

def is_session_active(uSession):
        if uSession.is_active():
                return True
        else:
                return login()
	
	

@get('/login')
def login():
		return '''<form action="/login" method="post">
		Username: <input name="username" type="text" />
		Password: <input name="password" type="password" />
		<input value="Login" type="submit" /></form>'''
	
	
@post('/login') # or @route('/login', method='POST')
def do_login():
	username = request.forms.get('username')
	password = request.forms.get('password')
	if check_login(username, password):
		new_session = Session(username)
		
		response.set_cookie('finplan_cookie',new_session.getSessionId(),max_age=3600)
		return finplan(True)
	else:
		return "<p>Login failed.</p>"

def check_login(username,password):
	global LOGGED_IN
	global USER_NAME
	if username == 'user':
		if password == 'pass':
			LOGGED_IN = 1
			USER_NAME = username
			return True
	return False
	
@route('/Add Client',method ='GET')
def new_Client():
	return template('NewClientDetails')


@route('/newclientdetails', method='POST')
def do_newclientdetails():
    # load form data
    firstname = request.forms.get('fname')
    lastname = request.forms.get('lname')
    addressline1 = request.forms.get('addln1')
    addressline2 = request.forms.get('addln2')
    town = request.forms.get('town')
    county = request.forms.get('county')
    eircode = request.forms.get('eircode')
    mphone = request.forms.get('mphone')
    ophone = request.forms.get('ophone')
    email = request.forms.get('email')
    dateofbirth = request.forms.get('dob')
    gender = request.forms.get('gender')

    # store form data
    conn = sqlite3.connect('FinplanDB.db')
    cursor = conn.cursor()
    query = '''INSERT INTO Client(firstname, lastname, dob, gender, addressline1, addressline2, town, county, eircode, email, mobile, landline) VALUES(?,?,?,?,?,?,?,?,?,?,?,?);'''
    values = (firstname, lastname, dateofbirth, gender, addressline1, addressline2, town, county, eircode, email, mphone, ophone)
    cursor.execute(query, values)
    conn.commit()
    client_select(cursor.lastrowid) 
    conn.close()
    return next_page('Add Client')
	
	
@route('/Income Details', method='GET')
def add_income():
	cookie = request.get_cookie('finplan_cookie')
	return template('Income_Details')

@route('/Insert_income_details',method ='POST')
def insertIncome():
	clientID = find_session(find_cookie()).get_value('client_id')
	
	income_count = int(request.POST.get("counter"))
	#get Values
	names =[]
	amounts =[]
	frequencies =[]
	durationStarts=[]
	durationEnds =[]
	for i in range(income_count):
		names.append(request.forms.get('name_%s' % str(i+1)).strip())
		amounts.append(request.forms.get('amount_%s' % str(i+1)))
		frequencies.append(request.forms.get('frequency_%s' % str(i+1)))
		durationStart = request.forms.get('DuationStart_%s',str(i+1)).strip()
		durationStarts.append(durationStart)
		durationEnd = request.forms.get('DuationEnd_%s',str(i+1)).strip()
		if durationEnd is not None:
			durationEnds.append(durationEnd)
		else:
			durationEnds.append(durationStart)
		
	conn = sqlite3.connect('FinplanDB.db')
	c = conn.cursor()
	for i in range(len(names)):
		c.execute("INSERT INTO Income_Details (Client_id,Description,Amount,Frequency,Duration_start,Duration_end) values (?,?,?,?,?,?)",(clientID,names[i],amounts[i],frequencies[i],durationStarts[i],durationEnds[i]))
	
	conn.commit()
	c.close()
	
	return next_page('Income Details')
	
@route('/Employment Details', method='GET')
def employment_Details():
	return template('Employment_Details')

@route('/Insert_employment_details',method='POST')
def insertEmployment():
    clientID = find_session(find_cookie()).get_value('client_id')
    employment_count=int(request.POST.get("counter"))
    names = []
    addresses = []
    telnos = []
    emails = []
    etypes = []
    durationStarts = []
    durationEnds = []
    for i in range(employment_count):
        names.append(request.forms.get('ename_%s' % str(i+1)))
        addresses.append(request.forms.get('address_%s' % str(i+1)))
        telnos.append(request.forms.get('telno_%s' % str(i+1)))
        emails.append(request.forms.get('eemail_%s' % str(i+1)))
        etypes.append(request.forms.get('etypes_%s' % str(i+1)))
        durationStart = request.forms.get('startdate_%s',str(i+1)).strip()
        durationStarts.append(durationStart)
        durationEnd = request.forms.get('enddate_%s',str(i+1)).strip()
        if durationEnd is not None:
            durationEnds.append(durationEnd)
        else:
            durationEnds.append(durationStart)
		
    conn = sqlite3.connect('FinplanDB.db')
    c = conn.cursor()
    for i in range(len(names)):
        c.execute("INSERT INTO Employment_Details (Client_id, Employer_name, Employer_address, Employer_phone, Employer_email, Employment_type, Duration_start, Duration_end) values (?,?,?,?,?,?,?,?)",(clientID,names[i],addresses[i],telnos[i],emails[i],etypes[i],durationStarts[i],durationEnds[i]))
	    #c.execute("INSERT INTO Employment_Details (Client_id, Employer_name, Employer_address, Employer_phone, Employer_email, Employment_type, Duration_start, Duration_end) values (?,?,?,?,?,?,?,?)",(USER_NAME,addresses[i],telnos[i],emails[i],etypes[i],durationStarts[i],durationEnds[i]))

    conn.commit()
    c.close()
	
    return next_page('Employment Details')
	


@route('/Expenditure Details', method='GET')
def expenditure_Details():
	return template('Expenditure_Details')
 
@route('/Insert_expenditure_details',method='POST')
def insertExpenditure():
    clientID = find_session(find_cookie()).get_value('client_id')
    expenditure_count=int(request.POST.get("counter"))
    names = []
    amounts = []
    frequencies = []
    durationStarts = []
    durationEnds = []
    for i in range(expenditure_count):
        names.append(request.forms.get('name_%s' % str(i+1)).strip())
        amounts.append(request.forms.get('amount_%s' % str(i+1)))
        frequencies.append(request.forms.get('frequency_%s' % str(i+1)))
        durationStart = request.forms.get('DuationStart_%s',str(i+1)).strip()
        durationStarts.append(durationStart)
        durationEnd = request.forms.get('DuationEnd_%s',str(i+1)).strip()
        if durationEnd is not None:
            durationEnds.append(durationEnd)
        else:
            durationEnds.append(durationStart)
		
    conn = sqlite3.connect('FinplanDB.db')
    c = conn.cursor()
    for i in range(len(names)):
        c.execute("INSERT INTO Expenditure_Details (Client_id,Description,Amount,Frequency,Duration_start,Duration_end) values (?,?,?,?,?,?)",(clientID,names[i],amounts[i],frequencies[i],durationStarts[i],durationEnds[i]))
	
    conn.commit()
    c.close()
	
    return next_page('Expenditure Details')
	

@route('/Product Types', method='GET')
def get_product_type():
	conn = sqlite3.connect('FinplanDB.db')
	c = conn.cursor()
	c.execute("SELECT * FROM Product_Type")
	result = c.fetchall()
	
	
	output = template('Product_types', products=result )

	return  output

@route('/Product Types',method='POST')
def set_product_type():
	product_id  = request.POST.get("product_id")
	find_session(find_cookie()).set_value('product_id',product_id)
	return next_page('Product Types')
	
@route('/Product Details', method='GET')
def get_product_details():
	productID = find_session(find_cookie()).get_value('product_id')
	userID = find_session(find_cookie()).get_value('user_id')
	clientID = find_session(find_cookie()).get_value('client_id')
	conn = sqlite3.connect('FinplanDB.db')
	cursor = conn.cursor()
	cursor.execute('SELECT * FROM Product_Questions WHERE product_id = ?;', (productID,))
	results = cursor.fetchall()
	cursor.execute('Select product_name, product_details FROM Product_Type WHERE id = ?;',(productID,))
	rows2 = cursor.fetchall()
	output = template('Product_Details', rows=results, product_id = productID, user_id = userID, client_id = clientID, product_details = rows2)
	conn.close()
	#Release 2 end
	return output
	
@route('/Product Details', method='POST')
def do_get_product_details():
# load form data
	productID = find_session(find_cookie()).get_value('product_id')
	userID = find_session(find_cookie()).get_value('user_id')
	clientID = find_session(find_cookie()).get_value('client_id')
	conn = sqlite3.connect('FinplanDB.db')
	cursor = conn.cursor()

	cursor.execute('''SELECT id, name FROM Product_Questions WHERE product_id = ?;''', (productID,))
	results = cursor.fetchall()
	
	for row in results:
		question_id = row[0]
		name = row[1]
		answerValue = request.POST.get(name)
		query = '''INSERT INTO Additional_Info(product_id, question_id, value, user_id, client_id) VALUES (?,?,?,?,?);'''
		values = (productID, question_id, answerValue, userID, clientID)
		cursor.execute(query, values)
		conn.commit()   
	conn.close()

	return next_page('Product Details')
	
@route('/Client Search', method='GET')
def client_search():
	return template('Client_Search')
	
@route('/Search_Results', method='POST')
def do_client_search():
    conn = sqlite3.connect('FinplanDB.db')
    cursor = conn.cursor()
    mode = request.forms.get('searchTypeSelection')
    if mode == 'Client Details':
        firstname = request.forms.get('firstName')
        lastname = request.forms.get('lastName')
        dob = request.forms.get('dob')
        if firstname == '':
            query = '''SELECT * FROM Client WHERE lastname LIKE ? AND dob=?;'''
            values = (lastname + "%", dob)
            cursor.execute(query, values)
            results = cursor.fetchall()
            return template('Client_Search_Results', rows=results)
        else:
            query = '''SELECT * FROM Client WHERE lastname LIKE ? AND firstName LIKE ? AND dob=?;'''
            values = (lastname + "%", firstname + "%", dob)
            cursor.execute(query, values)
            results = cursor.fetchall()
            return template('Client_Search_Results', rows=results)
    else:
        clientId = request.forms.get('clientId')
        query = '''SELECT * FROM Client WHERE clientId LIKE ?;'''
        values = (clientId + "%",)
        cursor.execute(query, values)
        results = cursor.fetchall()
        return template('Client_Search_Results', rows=results)
    conn.close()

@route('/Select_client',method ='POST')
def client_selected():
	client_id  = request.POST.get("client_id")
	client_select(client_id)
	return next_page('Client Select')
	
#for page navigation
@route('/action/product_select', method='POST')
def product_select(last_page,product):
	find_session(find_cookie()).set_value('product_id',product)
	return next_page(last_page)
	
	#hardcoded as login not in scope for release2
def user_select(user):
	find_session(find_cookie()).set_value('user_id',1)


def client_select(client):
	cookie = request.get_cookie('finplan_cookie')
	find_session(cookie).set_value('client_id',client)
	
def success():
	return template('Progress_success') 
#Navigation
def next_page(last_page):
		return{
		'Client Select':get_product_type(),
		'Add Client':employment_Details(),
		'Employment Details':add_income(),
		'Income Details':expenditure_Details(),
		'Expenditure Details':get_product_type(),
		'Product Types':get_product_details(),
		'Product Details':success()
		}.get(last_page,finplan())
	
#main code finplan accessible at http://localhost:8080/finplan
run(host='localhost',port=8080,debug=True)	
#run()	
