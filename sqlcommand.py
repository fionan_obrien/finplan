# -*- coding: utf-8 -*-
"""
Created on Sat Oct 15 11:29:29 2016

@author: Sean
"""

import sqlite3 as lite


class SqlCommand:
    
    def __init__(self, dbFile):
        self.openConnection(dbFile)
        
    def openConnection(self, dbFile):
        try:
            self.connection = lite.connect(dbFile)
            #print("Connection opened")
        except lite.Error as e:
            print("Exception: {}".format(e.args[0]))
    
    def closeConnection(self):
        if self.connection:
            self.connection.close()
            #print("Connection closed")
    
    def execute(self,query, values=None):
        cursor = self.connection.cursor()
        operation = query.split()
        operation = operation[0].strip().upper()
        try:
            if operation == 'SELECT':
                rows = cursor.execute(query, values)
                self.connection.commit()
                #print("Select command executed successfully ")
                return rows
            elif operation == 'INSERT':
                cursor.execute(query, values)
                self.connection.commit()
                #print("Insert command executed successfully ")
                return cursor.lastrowid
            elif operation == 'UPDATE':
                cursor.execute(query, values)
                self.connection.commit()
                #print("Update command executed successfully ")
            elif operation == 'DELETE':
                cursor.execute(query, values)
                self.connection.commit()
                #print("Delete command executed successfully ")
            else:
                cursor = self.connection.cursor()
                cursor.executescript(query)
                self.connection.commit()
                #print("DDL command executed successfully ")
        except lite.Error as e:
            if self.connection:
                self.connection.rollback()
                print("SqlException: {}".format(e.args[0]))
                #self.closeConnection()
 
            
if __name__ == "__main__":
#==============================================================================
#     command = SqlCommand('finplanDB.db')
#     command.execute('DROP TABLE IF EXISTS Friends')
#     command.execute('CREATE TABLE Friends(id INTEGER PRIMARY KEY, Name TEXT);')
#     parameters = ('Sean',)
#     val = command.execute('INSERT INTO Friends(Name) VALUES(?)', parameters)
#     command.execute('INSERT INTO Friends(Name) VALUES(?)', parameters)
#     print(val)
#     parameters = ('Sean',1)
#     command.execute('UPDATE Friends SET Name=? WHERE id=?', parameters)
#     rows = command.execute('SELECT * FROM Friends')
#     for row in rows:
#         print(row)
#     command.execute('DELETE FROM Friends WHERE id={}'.format(1))
#     command.execute('SELECT * FROM Friends')
#     for row in rows:
#         print(row)
#     command.closeConnection()
#==============================================================================
    pass